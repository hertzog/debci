#!/bin/sh

short_options='ot:p:i:'
long_options='print-output,trigger:,pin-packages:,run-id:'

usage() {
  cat <<EOF
usage: debci test [OPTIONS] srcpkg

Options:
  -o, --print-output
      print output directory after test finished
  -t TRIGGER, --trigger TRIGGER
      Records TRIGGER as being the trigger for this test run. The trigger will
      be written to a file in the output directory, so that it can be retrieved
      later
  -p RELEASE=pkgname,src:srcname,...
  --pin-packages RELEASE=pkgname,src:srcname,...
      Force specific packages to be installed from the given RELEASE. The
      format is the same as the --pin-packages option from autopkgtest. RELEASE
      will be automatically addded to the testbed APT sources.
  -i ID, --run-id ID
      Use the specific ID as the run id for this test run, instead of
      generating one based on the current date.
$@
EOF
}

set -eu

debci_base_dir=$(readlink -f $(dirname $(readlink -f $0))/..)
. $debci_base_dir/lib/environment.sh
. $debci_base_dir/lib/functions.sh

process_package() {
  # output directory for test-package/autopkgtest
  local base_dir="$(autopkgtest_incoming_dir_for_package "$pkg")"

  if [ -n "${run_id:-}" ]; then
    adt_out_dir="${base_dir}/${run_id}"
  else
    run_id=$(date +%Y%m%d_%H%M%S)
    adt_out_dir="${base_dir}/${run_id}"

    inc=0
    orig_run_id="$run_id"
    while [ -d "$adt_out_dir" ]; do
      # this in *very* unlikely to happen in production, but we need this for the
      # test suite
      run_id="${orig_run_id}.${inc}"
      adt_out_dir="${base_dir}/${run_id}"
    done
  fi

  mkdir -p "$(dirname $adt_out_dir)"
  start_timestamp=$(date +%s)


  # these variables can be considered as an API by backends/*/test-package and
  # debci-autopkgtest
  export debci_suite
  export debci_arch
  export debci_autopkgtest_args
  export debci_test_package="$pkg"
  export debci_test_outputdir="$adt_out_dir"

  if [ "$debci_quiet" = 'true' ]; then
    run_with_shared_lock "$debci_testbed_lock" \
      test-package $options --output-dir "$adt_out_dir" "$pkg" \
      >/dev/null 2>&1 || true
  else
    run_with_shared_lock "$debci_testbed_lock" \
      test-package $options --output-dir "$adt_out_dir" "$pkg" || true
  fi

  finish_timestamp=$(date +%s)

  find "$adt_out_dir" '(' \
    -name log -or \
    -name '*-stdout' -or \
    -name '*-stderr' ')' -exec gzip '{}' ';'

  if [ -f "${adt_out_dir}/duration".in ]; then
    cp "${adt_out_dir}/duration".in "${adt_out_dir}/duration"
  else
    echo $(($finish_timestamp - $start_timestamp)) > "$adt_out_dir/duration"
  fi

  if [ -n "$trigger" ]; then
    echo "$trigger" > "$adt_out_dir/trigger"
  fi

  if [ -n "$print_output" ]; then
      echo "$adt_out_dir"
  fi
}

# defaults
index=''
print_output=''
trigger=''
options=''

while true; do
  opt="$1"
  shift
  case "$opt" in
    -o|--print-output)
      print_output=true
      ;;
    -t|--trigger)
      trigger="$1"
      shift
      ;;
    -p|--pin-packages)
      pin="$1"
      shift
      release=$(echo "$pin" | cut -d = -f 1)
      options="$options --add-apt-release=$release --pin-packages=$pin"
      ;;
    -i|--run-id)
      run_id="$1"
      if echo "$run_id" | grep -q -e '^[0-9]\{8\}_[0-9]\{6\}$' -e '^[0-9]*$' ; then
        : ok
      else
        echo "E: invalid run-id: $run_id (format needs to be YYYYMMDD_HHMMSS)"
        exit 1
      fi
      shift
      ;;
    --)
      break
      ;;
  esac
done

if [ $# -eq 1 ]; then
  pkg="$1"
  process_package
else
  usage
  exit 1
fi
